# Project-GLAPS
This is a joint Cap Stone Project for Geographic Location Attribute Predictor System. This is only the front end.  

## Contributing



# Built With
- Python

# Project Manager
- Andrew Norris

# Advisor
- Andrew Norris

# Authors
- Brian Schweikart
- Jesse Watts

# Institution
Fayetteville Technical Community College
